# -*- coding: utf-8 -*-
#title          : nanolyse_handler.py
#description    : Python UI for nanolyse
#author_1       : Florian L. R. Lucas
#affiliation_1  : Groningen Biomolecular Sciences & Biotechnology Institute, University of Groningen
#author_2       : Carlos de Lannoy
#affiliation_2  : Department of Plant Sciences, Wageningen University
#cite_as        : 
#version        : 0.1
#date           : 20180329
#notes          : 
#python_version : 2.7.14
#==============================================================================

# Import modules
import pandas as pd
import numpy as np
from nanolyse import nanolyse

from xgboost.sklearn import XGBClassifier

import numpy.random as nprnd
from sklearn.cross_validation import train_test_split

class nanolyse_handler:
    def __init__( self ):
        self.x_names = ['L1 dwelltime',
                        'Ires',
                        'Ires S.D.2',
                        'dIres',
                        'dIres S.D',
                        'L1 Ires cumsum',
                        'L1 Ires cumsum S.D.',
                        'Ires min',
                        'Ires max',
                        'dIres min',
                        'dIres max',
                        'Label']
        # self.x_names = ['Ires',
        #                 'Ires S.D.2',
        #                 'L1 dwelltime',
        #                 'Label']
        self.y_names = ['Sequence']
    
    def load( self, A ):
        b = [  ]
        for a in A:
            nl = nanolyse(  )
            nl.loadAxon( a['file_name_baseline'] )
            nl.get_levels()
            nl.loadAxon( a['file_name'] )
            try:
                nl.get_events( dwelltime = a['dwelltime_min'], skip = a['skip'] ) if a['Setting']=='auto' else nl.get_events( l0=a['L0'], l1=a['L1'], dwelltime=a['dwelltime_min'], skip=a['skip'] )
            except:
                nl.get_events( dwelltime = a['dwelltime_min'], skip = a['skip'] )
            
            nl.get_meta()
            b.append( pd.concat([pd.DataFrame({'Label':[a['label']]*len(nl.meta), 'Sequence':[a['sequence']]*len(nl.meta),'pH':[a['pH']]*len(nl.meta)}, columns = ('Label','Sequence','pH')), nl.meta], axis=1) )
        for i in range( len( b ) ):
            if i==0:
                meta = b[ i ]
            else:
                meta = pd.concat( [ meta, b[ i ] ], axis=0 )
        return meta

    def load_squiggle(self, A, label_dict):
        labels = []
        signal = []
        seq_lengths = []

        for a in A:
            nl = nanolyse()
            nl.loadAxon(a['file_name_baseline'])
            nl.get_levels()
            nl.loadAxon(a['file_name'])
            try:
                nl.get_events(dwelltime=a['dwelltime_min'], skip=a['skip']) if a[
                                                                                   'Setting'] == 'auto' else nl.get_events(
                    l0=a['L0'], l1=a['L1'], dwelltime=a['dwelltime_min'], skip=a['skip'])
            except:
                nl.get_events(dwelltime=a['dwelltime_min'], skip=a['skip'])
            cur_labels = np.zeros(nl.signal.size)
            for b, e in zip(nl.L1_start, nl.L1_end):
                cur_labels[b:e] = label_dict[a['sequence']]
            labels.append(cur_labels)
            signal.append(nl.signal)
            seq_lengths.append(nl.signal.size)
        labels = np.concatenate(labels)
        signal = np.concatenate(signal)
        return signal, labels, seq_lengths
    
    def train_val_split( self ):
        # Unpack X and Y
        X, Y = np.array( zip( *self.data ) )

        # NOTE: added, see what happens if test set is held-out experiments
        exp_keywords = ['BLANK', 'Enkephalin [Leu]', 'Enkephalin [Met]']
        random_exp = []
        exp_label_check = [False] * len(exp_keywords)
        while not all(exp_label_check):
            random_exp = np.random.choice(self.experiments, len(exp_keywords))
            for il, lab in enumerate(exp_keywords):
                exp_label_check[il] = any([lab in exp for exp in random_exp])
        random_exp.sort()
        test_idx = []
        for i, xc in enumerate(X):
            if xc[-1] in random_exp:
                test_idx.append(i)

        for id in range(X.size):
            X[id] = X[id][:-1]

        X_5 = X[test_idx]
        y_5 = Y[test_idx]


        X_train, X_validate, X_test, y_train, y_validate, y_test = (np.array([]),np.array([]),np.array([]),np.array([]),np.array([]),np.array([]))
        # Unpack different labels
        X2, Y2, len_Y = ([],[], [])
        for i in self.labels:
            X2.append( X[np.where(Y==i)] )
            Y2.append( Y[np.where(Y==i)] )
            len_Y.append( len( np.where(Y==i)[0] ) )
        min_len = min( len_Y )
        self.X2 = X2
        self.Y2 = Y2
        for x, y, len_y in zip( X2, Y2, len_Y ):

            ts = self.test_size * ( float( min_len ) / float( len_y ) )
            ts2 = ts * ( len_y / float( len_y - int( len_y * ts ) ) )

            X_4, X_3, y_4, y_3 = train_test_split(x, y, test_size=ts)
            # X_5, X_4, y_5, y_4 = train_test_split(X_4, y_4, test_size=ts2)
            X_train = np.concatenate( ( X_train, X_3 ) )
            y_train = np.concatenate( ( y_train, y_3 ) )
            X_validate = np.concatenate( ( X_validate, X_4 ) )
            y_validate = np.concatenate( ( y_validate, y_4 ) )
            X_test = np.concatenate( ( X_test, X_5 ) )
            y_test = np.concatenate( ( y_test, y_5 ) )
        X_train = np.array( [ i for i in X_train ] )
        X_validate = np.array( [ i for i in X_validate ] )
        X_test = np.array( [ i for i in X_test ] )
        
        return X_train, X_validate, X_test, y_train, y_validate, y_test, random_exp
    
    def train(self, data_train, test_size=0.2, epochs=1):
        self.test_size, self.epochs = test_size, epochs
        self.labels = np.unique( data_train.as_matrix( self.y_names ) )
        self.experiments = np.unique(data_train.as_matrix(['Label']))
        
        X = np.array( data_train.as_matrix( self.x_names ) )
        Y = np.array( [i[0] for i in data_train.as_matrix( self.y_names )] )
        self.data = zip( X, Y )
        self.X = X
        self.Y = Y
        
        self.accuracy = []
        self.precision = []
        self.recall = []
        self.test_exps = []
        self.test_labels = []
        self.NA_fraction = np.zeros(epochs+1)
        
        
        self.error = []
        for i in range( epochs ):
            # Split data into train and validation set (such that each class is equally represented)
            X_test = np.array([])
            while X_test.size == 0:
                X_train, X_validate, X_test, y_train, y_validate, y_test, test_exps = self.train_val_split()
            self.X_train = X_train
            self.y_train = y_train
            self.xr = XGBClassifier(
                    learning_rate =0.1,
                    n_estimators=100,
                    max_depth=6,
                    min_child_weight=1,
                    gamma=0,
                    subsample=0.8,
                    colsample_bytree=0.8,
                    objective= 'multi:softprob',
                    scale_pos_weight=1)
            eval_set = [ ( X_validate, y_validate ) ]
            self.xr.fit( X_train, 
                        y_train, 
                        eval_metric="mlogloss",   # Make sure this is for multiple classes! not binary e.g. error use merror instead 
                        eval_set=eval_set, 
                        verbose=False)
            
            y_pred = self.predict2( X_test )
            # NOTE: prediction accuracies here only depend on xgbc
            acc = self.veracity( y_pred[ np.where( y_pred!='N/A' ) ], np.array( y_test )[ np.where( y_pred!='N/A' ) ] )
            pre = self.get_precision( y_pred[ np.where( y_pred!='N/A' ) ], np.array( y_test )[ np.where( y_pred!='N/A' ) ] )
            rec = self.get_recall(y_pred[np.where(y_pred != 'N/A')], np.array(y_test)[np.where(y_pred != 'N/A')])
            self.accuracy.append(acc)
            self.precision.append(pre)
            self.recall.append(rec)
            self.test_exps.append(test_exps)
            self.test_labels.append(self.labels.astype(list))
            # NOTE: added extra property to find out fraction of unclassified examples (xgbc spits out none)
            self.NA_fraction[i] = sum(y_pred=='N/A') / y_pred.size
        self.accuracy = np.array( self.accuracy )
        self.precision = np.array(self.precision)
        self.recall = np.array(self.recall)
        self.test_exps = np.array(self.test_exps)
        self.test_labels = np.array(self.test_labels)

    
    def predict( self, data ):
        try:
            X = data.as_matrix( self.x_names )
        except:
            X = np.array( data )
        #pred = self.xr.predict( X )
        #prob = self.xr.predict_proba( X )
        
        return self.xr.predict( X )
    
    def predict2( self, data ):
        try:
            X = data.as_matrix( self.x_names )
        except:
            X = np.array( data )
        prob = self.xr.predict_proba( X )
        y_pred = np.array( [self.labels[ np.argmax( i ) ] if np.sort( i )[-1]/ np.sort( i )[-2] > np.sqrt(np.exp(1)) else 'N/A' for i in prob ] )
        return y_pred
    
    def veracity(self, y_pred, y_val):
        # NOTE: this is recall
        c=[]
        for k in self.labels:
            try:
                if k in y_val:
                    c.append( len([i for i, j in
                                   zip(np.array( y_pred )[np.where(np.array( y_val )==k)],
                                       np.array( y_val )[np.where(np.array( y_val )==k)])
                                   if i == j]) /
                              float( len( np.array( y_pred )[np.where(np.array( y_val )==k)] ) ) )
                else:
                    c.append(None)
            except:
                c.append( 0 )
        return c

    def get_recall(self, y_pred, y_val):
        c = []
        y_pred = np.array(y_pred); y_val = np.array(y_val)
        for k in self.labels:
            try:
                if k in y_val:
                    c.append(len([i for i, j in
                                  zip(y_pred[np.where(y_val == k)],
                                      y_val[np.where(y_val == k)])
                                  if i == j]) /
                             float(np.sum(y_val == k)))
                else:
                    c.append(None)
            except:
                c.append(0)
        return c

    def get_precision(self, y_pred, y_val):
        c = []
        y_pred = np.array(y_pred); y_val = np.array(y_val)
        for k in self.labels:
            try:
                if k in y_val:
                    c.append(len([i for i, j in
                                  zip(y_pred[np.where(y_val == k)],
                                      y_val[np.where(y_val == k)])
                                  if i == j]) /
                             float(np.sum(y_pred == k)))
                else:
                    c.append(None)
            except:
                c.append(0)
        return c
