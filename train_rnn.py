from ClassBidirectionalRnn import ClassBidirectionalRnn
import pandas as pd
import yaml
import numpy as np
import sys
import os

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
# -- Prepare save location ---

save_location = sys.argv[1]
df_fn = sys.argv[2]


if not os.path.isdir(save_location):
    os.mkdir(save_location)
save_location = os.path.realpath(save_location) + '/'

train_performance_fn = save_location + 'train_performance.tsv'
with open(train_performance_fn, 'w') as pf:
    pf.write('example\tYGGFL_pre\tYGGFL_rec\tYGGFM_pre\tYGGFM_rec\n')

test_performance_fn = save_location + 'test_performance.tsv'
with open(test_performance_fn, 'w') as pf:
    pf.write('epoch\tYGGFL_pre\tYGGFL_rec\tYGGFM_pre\tYGGFM_rec\n')

model_ckpt_fn = save_location + 'protDiscrimination_YGGFLvsYGGFM.ckpt'
if os.path.isfile(model_ckpt_fn):
    os.remove(model_ckpt_fn)

# --- Prepare data ---
df = pd.read_pickle(df_fn)
with open(__location__ + '/RnnParameterFile_defaults.yaml') as pf:
    params = yaml.load(pf)

# map file names to labels
abf_protein_dict = {
    'enkephalin_Leu0001.abf': 'YGGFL',
    'enkephalin_Leu0002.abf': 'YGGFL',
    'enkephalin_Leu0003.abf': 'YGGFL',
    'enkephalin_Met0000.abf': 'YGGFM',
    'enkephalin_Met0001.abf': 'YGGFM',
    'enkephalin_Met0002.abf': 'YGGFM'
}
df['protein'] = [abf_protein_dict[cabf] for cabf in df.index]
nb_unique_proteins = len(set(abf_protein_dict.values()))

# map labels to onehot
protein_onehot_dict = {
    'YGGFL': [0, 1],
    'YGGFM': [1, 0]
}

df['onehot'] = df.apply(lambda x: np.vstack((x.label,
                                             np.zeros((nb_unique_proteins-1, x.label.size), dtype=np.int64))
                                            )[protein_onehot_dict[x.protein], :],
                        axis=1)

# Prepare RNN
rnn = ClassBidirectionalRnn(**params)
rnn.initialize_model(params=None)

# Train
for ep in range(params['num_epochs']):

    # Stratified sampling
    df_train = df.groupby('protein', group_keys=False).apply(lambda x: x.sample(min(len(x), 2)))  # TODO: generalize
    df_test = df.loc[np.invert(np.in1d(df.index, df_train.index))]
    nb_train_tuples = df_train.shape[0]
    next_print_idx = 0
    tr_index = 0
    for batch_idx in range(params['num_batches']):
        tr_squiggle, tr_onehot = rnn.generate_random_subset(df)
        loss = rnn.train_model(tr_squiggle, tr_onehot)

        tr_index += params['batch_size']
        if tr_index >= next_print_idx:
            next_print_idx += 500
            confusion_dict = rnn.evaluate_model(tr_squiggle, tr_onehot)
            print("%d examples processed, loss %f. per class performance: " % (tr_index, loss))
            for cl in confusion_dict:
                print('{cls}: precision {precision} recall {recall}'.format(cls=cl+1,
                                                                            precision=confusion_dict[cl]['precision'],
                                                                            recall=confusion_dict[cl]['recall']))
            with open(train_performance_fn, 'a') as pf:
                pf.write('{ex}\t{cl1_pre}\t{cl1_rec}\t{cl2_pre}\t{cl2_rec}\n'.format(
                    ex=tr_index,
                    cl1_pre=confusion_dict[0]['precision'],
                    cl1_rec=confusion_dict[0]['recall'],
                    cl2_pre=confusion_dict[1]['precision'],
                    cl2_rec=confusion_dict[1]['recall'],
                ))

    ts_squiggle, ts_onehot = rnn.generate_random_subset(df_test)
    confusion_dict = rnn.evaluate_model(ts_squiggle, ts_onehot)
    print("end of epoch {}, test performance: ".format(ep))
    for cl in confusion_dict:
        print('{cls}: precision {precision} recall {recall}'.format(cls=cl + 1,
                                                                    precision=confusion_dict[cl]['precision'],
                                                                    recall=confusion_dict[cl]['recall']))
    with open(test_performance_fn, 'a') as pf:
        pf.write('{ep}\t{cl1_pre}\t{cl1_rec}\t{cl2_pre}\t{cl2_rec}\n'.format(
            ep=ep,
            cl1_pre=confusion_dict[0]['precision'],
            cl1_rec=confusion_dict[0]['recall'],
            cl2_pre=confusion_dict[1]['precision'],
            cl2_rec=confusion_dict[1]['recall'],
        ))
    rnn.save_checkpoint(model_ckpt_fn)
