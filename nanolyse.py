# -*- coding: utf-8 -*-
#title          : nanolyse.py
#description    : Nanolyse, script to analyse electrophysiology data
#author_1       : Florian L. R. Lucas
#affiliation_1  : Groningen Biomolecular Sciences & Biotechnology Institute, University of Groningen
#author_2       : Carlos de Lannoy
#affiliation_2  : Department of Plant Sciences, Wageningen University
#cite_as        : 
#version        : 0.1
#date           : 20180329
#notes          : 
#python_version : 2.7.14
#==============================================================================

# Import modules
from neo.io import AxonIO
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
from scipy import asarray as ar,exp
from scipy.stats import skew, kurtosis

class nanolyse:
    def __init__( self ):
        pass
    
    ##################################
    #   Load data from .abf format   #
    ##################################
    def loadAxon( self, fname ):
        bl = AxonIO( filename=fname ).read()
        self.signal = [ np.array( seg.analogsignals[0] )[:,0] for seg in bl[0].segments ][0] # Signal Trace
        self.sampling_period = [ np.asscalar( bl[0].segments[0].analogsignals[0].sampling_period ) for seg in bl[0].segments ][0] # Time between datapoints
    
    ####################################################
    #   Get events of L1 based on half signal method   #
    ####################################################
    def get_events( self, l0=False, l1=False, dwelltime=0, skip=2 ):
        try:
            n_filter = dwelltime / float( self.sampling_period ) if dwelltime / float( self.sampling_period ) > 2 else 2    # While dwelltime suggests that also spikes can be seen, atleast 2 datapoints are required to be an event
            if l0==False and l1==False: l0 = self.l0; l1 = self.l1;                                                         # Check if levels where defined, else try to get it from get_levels save
            a = np.where( abs( self.signal ) < abs( l1 ) + ( ( abs( l0 ) - abs(l1) ) / 2 ) )[0]                             # All datapoints above the threshold
            L1_end = np.where( np.diff( a ) > skip )[0]                                                                     # Get all event starts (e.g. where two datapoints are maximum 'skip' apart)
            L1_start = np.delete( np.insert(L1_end+1, 0, 0), -1, 0 )                                                        # Every end is followed by a new beginning
            idx = np.where( L1_end - L1_start > n_filter )[0]                                                               # Only keep those events that are atleast 2 or n_filter data points long
            self.L1_start, self.L1_end = a[ L1_start[ idx ] ], a[ L1_end[ idx ] ]                                           # Set L1 with datapoints that are allowed
            self.L0_start, self.L0_end = np.delete( np.insert(self.L1_end+1, 0, 0), -1, 0 ), self.L1_start                  # Set L0 relative to L1
            self.L0 = np.array( [ self.signal[i:j] for i, j in zip( self.L0_start, self.L0_end ) ] )                        # Add the signal of L0
            self.L1 = np.array( [ self.signal[i:j] for i, j in zip( self.L1_start, self.L1_end ) ] )                        # Add the signal of L1
        except:
            print "Unable to get events: Check parameters (e.g. levels)"

    #####################################################################
    #   Get levels of L0 and L1 based on the lowest Gaussian method     #
    #####################################################################
    def get_levels( self, window = 10, sigma = 1 ):
        try:
            def gaus(x,a,x0,sigma): return a*exp(-(x-x0)**2/(2*sigma**2))
            sort_signal = np.sort( np.abs( self.signal ) * -1 ); 
            d_sort_signal = np.diff( sort_signal ); 
            s_1 = 0; i = 0; f=0
            while sum( d_sort_signal[i*window:(i+1)*window] ) < s_1 or \
                    i==0 or \
                    f<10:
                s_1 = sum( d_sort_signal[i*window:(i+1)*window] ); i+=1; f+=1;
            bins = np.linspace( min( self.signal ), max( self.signal ), 1000 )
            hist = np.histogram( np.abs( self.signal ) * -1, bins=bins )
            
            popt,pcov = curve_fit(gaus,
                                  np.diff(hist[1]) + hist[1][0:-1],
                                  hist[0],
                                  p0=[1,sort_signal[i*window],1])
            self.l0_dist = popt
            self.l0 = popt[1] * np.sign( sum( self.signal ) ) * -1; 
            self.l1 = ( popt[1] + ( abs( popt[2] ) * sigma ) * 2 ) * np.sign( sum( self.signal ) ) * -1
            
        except:
            print("Unable to get levels")


    #########################################
    # The following code gets the meta data #
    #########################################
    def get_meta(self):
        L0_mean, L1_mean, L0_SD, L1_SD = np.array( zip(*[( np.mean(i), np.mean(j), np.std(i), np.std(j) ) for i, j in zip( self.L0, self.L1 ) ] ) )
        
        L0_Ires, L1_Ires = np.array( zip(*[(k/j, i/j) for k, i, j in zip(self.L0, self.L1, L0_mean)] ) )
        Ires = L1_mean / L0_mean
        
        Cov_L0_L1 = np.cov( L0_mean, L1_mean )[0][1]
        Ires_SD_2 = ( Ires**2 * ( (L0_SD/L0_mean)**2 + (L1_SD/L1_mean)**2 - 2*(Cov_L0_L1/(L0_mean*L1_mean)) ) )
        dIres, dIres_std = np.array( zip(*[ ( np.mean( np.diff(i) ), np.std( np.diff(i) ) ) for i in L1_Ires ] ) )
        
        L0_Ires_cumsum, L1_Ires_cumsum = np.array( zip(*[( np.cumsum( i ), np.cumsum(j) ) for i, j in zip( L0_Ires, L1_Ires ) ] ) )
        L0_mean_cumsum, L1_mean_cumsum, L0_SD_cumsum, L1_SD_cumsum = np.array( zip(*[( np.mean( i ), np.mean( j ), np.std( i ), np.std( j ) ) for i, j in zip( L0_Ires_cumsum, L1_Ires_cumsum ) ] ) )
        
        
        L1_dwelltime = (self.L1_end-self.L1_start) * self.sampling_period
        
        Ires_min, Ires_max = np.array( zip(*[ ( np.min( i ), np.max(i) ) for i in L1_Ires ] ) )
        dIres_min, dIres_max = np.array( zip(*[ ( np.min( np.diff( i ) ), np.max( np.diff( i ) ) ) for i in L1_Ires ] ) )
        
        self.meta = pd.DataFrame(
                {'L0 mean':L0_mean,
                 'L1 mean':L1_mean,
                 'L0 Ires':L0_Ires, 
                 'L1 Ires':L1_Ires, 
                 'Ires':Ires, 
                 'Ires S.D.2':Ires_SD_2, 
                 'Ires min':Ires_min,
                 'Ires max':Ires_max,
                 'dIres min':dIres_min,
                 'dIres max':dIres_max,
                 'dIres':dIres, 
                 'dIres S.D':dIres_std,
                 'L0 Ires cumsum':L0_mean_cumsum,
                 'L1 Ires cumsum':L1_mean_cumsum,
                 'L0 Ires cumsum S.D.':L0_SD_cumsum,
                 'L1 Ires cumsum S.D.':L1_SD_cumsum,
                 'L1 dwelltime':L1_dwelltime,
                },columns = (
                           'L0 mean',
                           'L1 mean',
                           'L0 Ires',
                           'L1 Ires',
                           'Ires',
                           'Ires S.D.2',
                           'Ires min',
                           'Ires max',
                           'dIres min',
                           'dIres max',
                           'dIres',
                           'dIres S.D',
                           'L0 Ires cumsum',
                           'L1 Ires cumsum',
                           'L0 Ires cumsum S.D.',
                           'L1 Ires cumsum S.D.',
                           'L1 dwelltime',
                           )
                ).dropna()