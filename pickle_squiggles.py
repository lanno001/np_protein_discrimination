import argparse
import pandas as pd
import numpy as np
from neo.io import AxonIO
import helper_functions as hp
from os.path import basename

parser = argparse.ArgumentParser(description='Detect events in squiggles and save them in a pickled pandas dataframe.')
parser.add_argument('abf', nargs='+', help='abf file(s)')
parser.add_argument('-o', '--out-file', required=True, type=str,
                    help='output file name')
parser.add_argument('--event-table', action='store_true',
                    help='Store event table instead of whole squiggle.')
args = parser.parse_args()

nb_abfs = len(args.abf)
abf_paths = args.abf
abf_list = [basename(ca) for ca in args.abf]

if args.event_table:
    df_out_list = []
else:
    df_out = pd.DataFrame({'abf': abf_list,
                           'ires': [np.array([])] * nb_abfs,
                           'label': [np.array([])] * nb_abfs},
                          columns=['abf', 'ires', 'label']).set_index('abf')
for abf, abf_fn in zip(abf_paths, abf_list):
    bl = AxonIO(filename=abf).read()
    squiggle = [np.array(seg.analogsignals[0])[:, 0] for seg in bl[0].segments][0]  # Signal Trace
    sampling_period = [np.asscalar(bl[0].segments[0].analogsignals[0].sampling_period) for seg in bl[0].segments][0]
    hmm_labels = hp.classify(squiggle, method='hmm')
    ires = hp.calculate_ires(squiggle, hmm_labels)
    if args.event_table:
        events_ires_vec = []
        hmm_labels_condensed = hp.condense_vector(hmm_labels)
        for event in hmm_labels_condensed:
            if event[1] != 0:
                events_ires_vec.append(ires[event[0]])
        df_out_list.append(pd.DataFrame({'abf': [abf_fn]*len(events_ires_vec),
                                         'ires': events_ires_vec
                                         }, columns=['abf', 'ires']))
    else:
        df_out.loc[abf_fn] = {'ires': ires, 'label': hmm_labels}
if args.event_table:
    df_out = pd.concat(df_out_list, ignore_index=True)
df_out.to_pickle(args.out_file)
