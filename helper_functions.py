from bokeh.models import ColumnDataSource, LinearColorMapper, Range1d
from bokeh.plotting import figure
import numpy as np
from scipy import exp
from scipy.optimize import curve_fit
from neo.io import AxonIO
import os
from hmmlearn import hmm
import itertools
import numba as nb
# from joblib import Parallel
import warnings
from scipy.signal import argrelextrema
from scipy.ndimage.filters import gaussian_filter

__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
categorical_colors = ['#8dd3c7', '#ffffb3', '#bebada', '#fb8072']
continuous_colors = ['#ffffff', '#fff7ec', '#fee8c8', '#fdd49e', '#fdbb84',
                     '#fc8d59', '#ef6548', '#d7301f', '#990000']
wur_colors = ['#E5F1E4', '#3F9C35']
white_green_colors = ['#ffffff', '#3F9C35']
white_blue_colors = ['#ffffff', '#0000ff']
white_yellow_colors = ['#ffffff', '#FFFF00']
white_red_colors = ['#ffffff', '#ff0000']


def classify(squiggle, sampling_period, method='lowest_gauss', baseline=None):
    # squiggle = filter_gaussian(squiggle, sampling_period, 10000)
    if method == 'hmm':
        labels, _ = hmm_classification(squiggle)
    elif method == 'lowest_gauss':
        n_filter = 2
        skip = 2
        if baseline is None:
            l0, l1 = get_lowest_gauss_levels(signal=squiggle)
        else:
            bl = AxonIO(filename=baseline).read()
            bl_squiggle = [np.array(seg.analogsignals[0])[:, 0] for seg in bl[0].segments][0]
            l0, l1 = get_lowest_gauss_levels(signal=bl_squiggle)

        signal = squiggle
        dwelltime = 0
        skip = 2
        trace = 0
        t0 = 0
        t1 = -1

        n_filter = dwelltime / float(sampling_period) if dwelltime / float(
            sampling_period) > 2 else 2  # While dwelltime suggests that also spikes can be seen, atleast 2 datapoints are required to be an event
        a = np.where(abs(signal) < abs(l1))[0]  # All datapoints above the threshold
        L1_end = np.where(np.diff(a) > skip)[
            0]  # Get all event starts (e.g. where two datapoints are maximum 'skip' apart)
        L1_end = np.append(L1_end, len(a) - 1)
        L1_start = np.delete(np.insert(L1_end + 1, 0, 0), -1, 0)  # Every end is followed by a new beginning
        idx = np.where(L1_end - L1_start > n_filter)[
            0]  # Only keep those events that are atleast 2 or n_filter data points long
        L1_start, L1_end = a[L1_start[idx]], a[L1_end[idx]]  # Set L1 with datapoints that are allowed
        L0_start, L0_end = np.delete(np.insert(L1_end + 1, 0, 0), -1, 0), L1_start  # Set L0 relative to L1
        L0 = np.array([signal[i:j] for i, j in zip(L0_start, L0_end)])  # Add the signal of L0
        L1 = np.array([signal[i:j] for i, j in zip(L1_start, L1_end)])  # Add the signal of L1


        # n_filter = dwelltime / float(sampling_period) if dwelltime / float(
        #     sampling_period) > 2 else 2  # While dwelltime suggests that also spikes can be seen, atleast 2 datapoints are required to be an event
        # a = np.where(abs(signal) < abs(l1))[0]  # All datapoints above the threshold
        # b = np.where(np.diff(a) > skip)[0]  # Get all event starts (e.g. where two datapoints are maximum 'skip' apart)
        # c = np.delete(np.insert(b + 1, 0, 0), -1, 0)  # Every end is followed by a new beginning
        # idx = np.where(b - c > n_filter)[0]  # Only keep those events that are atleast 2 or n_filter data points long
        # L1_start, L1_end = a[c[idx]], a[b[idx]]  # Set L1 with datapoints that are allowed
        # L0_start, L0_end = np.delete(np.insert(L1_end + 1, 0, 0), -1,
        #                                        0), L1_start  # Set L0 relative to L1
        # L0 = np.array([signal[i:j] for i, j in zip(L0_start, L0_end)])  # Add the signal of L0
        # L1 = np.array([signal[i:j] for i, j in zip(L1_start, L1_end)])

        labels = np.zeros_like(squiggle, dtype=int)
        for s, e in zip(L1_start, L1_end):
            labels[s:e] = 1
    return labels


def get_lowest_gauss_levels(signal, window=10, sigma=1, trace=0, t0=0, t1=-1):
    signal = signal[t0:t1]
    mu, std, res = gauss_deconv(signal)
    l0 = mu * np.sign(sum(signal)) * -1
    l1 = (mu + (abs(std) * sigma) * 2) * np.sign(sum(signal)) * -1
    return l0, l1


def gauss(x, *p):
    a, mu, sigma = p
    return a * np.exp(-(x - mu) ** 2 / float(2 * sigma ** 2))


def gauss_deconv(signal, n_peaks=2):
    try:
        bins = np.linspace(min(signal), max(signal), 2000)  # Bins the signal
        hist = np.histogram(np.abs(signal) * -1, bins=bins)  # Histogram sthe signal
        means, stds = ([], [])  # Initialise variables
        x = np.diff(hist[1]) + hist[1][0:-1]  # Calculate the x coördinates
        res = hist[0].astype('float64')  # Type cast the corresponding y values
        try:
            for i in range(n_peaks):
                max_bin = np.where(max(res) == res)[0][0]  # Bin width the most counts
                coeff, var_matrix = curve_fit(gauss,  # Fit Gaussian peak
                                              x[max_bin - 10:max_bin + 10],
                                              res[max_bin - 10:max_bin + 10],
                                              p0=[max(res), x[max_bin], 1])
                res -= gauss(x, *coeff)  # Subtract the Gaussian peak calculated
                a, mu, s = coeff  # Unzip variables
                means.append(mu)
                stds.append(s)
        except:
            print("Unexpected error during Gaussian deconvolve")
        finally:
            return means[np.argmax(np.abs(means))], stds[np.argmax(np.abs(means))], res
    except:
        print("Unexpected error: unable to load data")


def normalize(raw, method='mad'):
    if method is 'mad':
        raw_median = np.median(raw)
        mad = np.sum(np.abs(raw - raw_median))
        raw_normalized = (raw - np.median(raw)) / mad
    else:
        raise ValueError('method not implemented')
    return raw_normalized


@nb.njit(parallel=True)
def rolling_ttest(vec, ws=15):
    ttest_vec = np.ones(vec.size, dtype=nb.float32)
    fac = np.sqrt(2/ws)
    for idx in nb.prange(vec.size - ws):
        s1 = vec[idx:idx + ws]
        s2 = vec[idx + ws:idx + 2 * ws]
        pooled_std = np.sqrt((np.std(s1) + np.std(s2)) / 2)
        t_val = (np.mean(s1) - np.mean(s2)) / (fac * pooled_std)
        ttest_vec[idx+ws] = t_val
    return ttest_vec


def condense_vector(vec):
    cur_val = vec[0]
    cur_idx = []
    out_vec = []
    for i, v in enumerate(vec):
        if v == cur_val:
            cur_idx.append(i)
        else:
            out_vec.append([cur_idx, cur_val])
            cur_val = v
            cur_idx = [i]
    out_vec.append([cur_idx, cur_val])
    return out_vec


def expand_vector(tup_list):
    return np.array(list(itertools.chain.from_iterable([[tup[1]] * len(tup[0]) for tup in tup_list])))


def calculate_ires(squiggle, label):
    label_condensed = condense_vector(label)
    cur_open_mean = 1
    cur_open_idx = []
    for event in label_condensed:
        if event[1] == 0:
            cur_open_mean = np.mean(squiggle[event[0]])
            cur_open_idx = event[0]
        else:
            cur_open_idx.extend(event[0])
            squiggle[cur_open_idx] = squiggle[cur_open_idx] / cur_open_mean
    return squiggle


def filter_gaussian(signal, sampling_period, Fs):
    """Gaussian filter for signal.

    Filters input signal using a gaussian filer and a user defined cut-off frequency.

    Parameters
    ----------
    singal : numpy array
        The signal should be fed as a array of arrays.
        Each top-level array is threated as a trace of a signal allowing easy cross-trace analysis (for e.g. current dependent analysis).
    sampling_period : float
        The sampling_frequency is used in combination with the dwelltime argument exclude short events.
    Fs : int
        The cut-off frequency in hertz

    Returns
    -------
    Numpy array
        Returns a numpy array, equal dimensions as the input signal

    """
    sigma = 1 / (sampling_period * Fs * 2 * np.pi)
    return gaussian_filter(signal, sigma)



# @nb.njit(parallel=True)
def hmm_clean_signal(hmm_ttest_mat):
    cur_val = hmm_ttest_mat[0, 0]
    start_idx = np.int32(0)
    ttest_val = 0
    nb_measurements = hmm_ttest_mat.shape[0]
    last_idx = nb_measurements - 1
    out_vec = np.zeros(nb_measurements, dtype=np.int64)
    accepted_ttest_vals = list(range(1, 3))
    for idx in nb.prange(hmm_ttest_mat.shape[0]):
        if hmm_ttest_mat[idx,0] == cur_val:
            ttest_val += hmm_ttest_mat[idx,1]
        else:
            if idx - start_idx > 3 and ttest_val in accepted_ttest_vals:
                out_vec[start_idx:idx] = cur_val
            cur_val = hmm_ttest_mat[idx, 0]
            start_idx = idx
            ttest_val = hmm_ttest_mat[idx, 1]
    # if ttest_val > 1 and last_idx - start_idx > 2:
    if last_idx - start_idx > 2:
        out_vec[start_idx:] = hmm_ttest_mat[-1, 0]
    return out_vec


def hmm_fun(vec_sub):
    hmm_obj = hmm.GaussianHMM(n_components=2, covariance_type='full', n_iter=10)
    with warnings.catch_warnings():
        warnings.simplefilter('ignore')
        if vec_sub.size > 10000:
            hmm_obj.fit(vec_sub[:50000].reshape(-1, 1))
        block_state = np.argmax(hmm_obj.means_)
        chunk_labels = (hmm_obj.predict(vec_sub.reshape(-1, 1)) == block_state).astype(np.int64)
    return chunk_labels


def hmm_classification(vec):
    vec_idx = np.arange(vec.size)
    vec_norm = (vec - np.mean(vec)) / np.std(vec)
    lin_coeff = np.linalg.lstsq(vec_idx.reshape(-1, 1), vec_norm)[0][0]
    vec_norm = vec_norm - vec_idx * lin_coeff

    nb_batches = vec.size // 1000000
    hmm_labels = [hmm_fun(vec_sub) for vec_sub in np.array_split(vec_norm, nb_batches)]
    hmm_labels = np.concatenate(hmm_labels)

    # numba-parallelized steps
    # initialize with little data to compile (as first run may not be parallelized)
    # ws = 15
    # _ = rolling_ttest(vec[:30], ws=ws)
    # ttest_sq = np.square(rolling_ttest(vec, ws=ws))
    # ttest_high_idx = argrelextrema(ttest_sq, np.less, order=10)[0]
    # ttest_high_vec = np.zeros_like(hmm_labels)
    # ttest_high_vec[ttest_high_idx] = 1
    # hmm_ttest_mat = np.stack((hmm_labels, ttest_high_vec), axis=1)
    # hmm_labels_clean = hmm_clean_signal(hmm_ttest_mat)
    # return hmm_labels_clean, ttest_sq
    return hmm_labels, None

def plot_timeseries(raw, y_hat=None, hmm_labels=None,
                    start=0, max_nb_points=100000, subset_type='end',
                    title='time series', freq=50000, debug_hmm=False):
    time = np.linspace(0, raw.size / freq, num=raw.size)
    graph_start = start / freq
    graph_end = (start + 10000) / freq

    # Uncomment for hmm classification
    if debug_hmm:
        hmm_labels, ttest = hmm_classification(raw)
        ttest_high_idx = argrelextrema(ttest, np.less, order=10)[0]
        ttest_time = time[ttest_high_idx]
        ttest_high_val = ttest[ttest_high_idx]

    # Get subset of data if required
    if raw.size > max_nb_points:
        if subset_type == 'start':
            subset_idx = list(range(max_nb_points))
            time = time[:max_nb_points]
            raw = raw[subset_idx]
        elif subset_type == 'end':
            subset_idx = list(range(raw.size-max_nb_points, raw.size))
            time = time[:max_nb_points]
            raw = raw[subset_idx]
        elif subset_type == 'subsampling':
            subset_idx = np.linspace(0, raw.size, num=max_nb_points).astype(int)[:-1]
            time = time[subset_idx]
            raw = raw[subset_idx]
        if debug_hmm:
            ttest_high_idx = np.intersect1d(ttest_high_idx, subset_idx)
            ttest_time = time[ttest_high_idx]
            ttest_high_val = ttest[ttest_high_idx]
            hmm_labels = hmm_labels[subset_idx]
            ttest = ttest[subset_idx]
        if y_hat is not None:
            y_hat = y_hat[subset_idx]
    y_range = raw.max() - raw.min()

    # Main data source
    cds_dict = dict(raw=raw,
                    time=time
                    )
    if y_hat is not None:
        cds_dict['cat'] = y_hat
        cds_dict['cat_height'] = np.repeat(np.mean(raw), len(y_hat))
        cds_dict['cat_height2'] = np.repeat(np.mean(raw)+0.5*y_range, len(y_hat))
    if debug_hmm:
        cds_dict['cat_hmm'] = hmm_labels
    source = ColumnDataSource(cds_dict)

    # Plotting
    ts_plot = figure(title=title)
    ts_plot.grid.grid_line_alpha = 0.3
    ts_plot.xaxis.axis_label = 't (s)'
    ts_plot.yaxis.axis_label = 'current signal'

    if y_hat is not None:
        col_mapper = LinearColorMapper(palette=white_blue_colors, low=0, high=y_hat.max())
        ts_plot.rect(x='time', y='cat_height', width=1.05/freq, height=y_range * 0.5, source=source, alpha=0.8,
                     fill_color={
                         'field': 'cat',
                         'transform': col_mapper
                     },
                     line_color=None)
    if debug_hmm:
        col_mapper2 = LinearColorMapper(palette=white_red_colors, low=0, high=y_hat.max())
        ts_plot.rect(x='time', y='cat_height2', width=1.05 / freq, height=y_range * 0.5, source=source, alpha=0.8,
                     fill_color={
                         'field': 'cat_hmm',
                         'transform': col_mapper2
                     },
                     line_color=None)
    ts_plot.line(x='time', y='raw', color='grey', line_width=1, source=source)
    # ts_plot.scatter(x='time', y='raw', color='grey', source=source)
    if debug_hmm:
        ts_plot.line(x=time, y=ttest, color='blue')
        ts_plot.scatter(x=ttest_time, y=ttest_high_val)
    ts_plot.plot_width = 1700
    ts_plot.plot_height = 500
    ts_plot.x_range = Range1d(graph_start, graph_end)
    return ts_plot
