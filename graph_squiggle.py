import argparse
from neo.io import AxonIO
import numpy as np
import helper_functions as hp
from bokeh.plotting import show, output_file, save

parser = argparse.ArgumentParser(description='Draw squiggle stored in an abf file.')
parser.add_argument('abf', help='abf file')
parser.add_argument('--classify', required=False, type=str,
                    help='classify sguiggle using given method and show classification. '
                         'May be one of the following: hmm, lowest_gauss (default: none).')
parser.add_argument('--baseline', required=False, type=str, default=None,
                    help='if classifying using lowest gaussian, use to determine levels')
parser.add_argument('--save-plot', required=False, type=str,
                    help='if provided, save plot at given location.')
parser.add_argument('--subset-type', required=False, default='start', type=str,
                    choices=['start', 'end', 'subsampling', 'None'],
                    help='If subset needs to be displayed, define whether it should take the '
                         'subset from the start, end or at equally interspaced distances of the '
                         'original squiggle (default: start)')
parser.add_argument('--apply-gauss-filter', type=int, default=None,
                    help='If supplied, applies gaussian filter of given frequency')
parser.add_argument('--debug-hmm', action='store_true', default=False,
                    help='Draw both the commandline-specified classification and the HMM-based in the graph.')
args = parser.parse_args()

bl = AxonIO(filename=args.abf).read()
squiggle = [np.array(seg.analogsignals[0])[:, 0] for seg in bl[0].segments][0]  # Signal Trace
sampling_period = [np.asscalar(bl[0].segments[0].analogsignals[0].sampling_period) for seg in bl[0].segments][0]  # Time between datapoints
if args.apply_gauss_filter is not None:
    squiggle = hp.filter_gaussian(squiggle, sampling_period, args.apply_gauss_filter)
if args.classify:
    labels = hp.classify(squiggle, method=args.classify, baseline=args.baseline, sampling_period=sampling_period)
    # squiggle_norm = hp.normalize(squiggle)
    plot_obj = hp.plot_timeseries(squiggle, y_hat=labels,
                                  title=args.abf,
                                  freq=1/sampling_period,
                                  subset_type=args.subset_type,
                                  debug_hmm=args.debug_hmm)
else:
    plot_obj = hp.plot_timeseries(squiggle, title=args.abf, freq=1/sampling_period, subset_type=args.subset_type)
if args.save_plot:
    output_file(args.save_plot)
    save(plot_obj)
else:
    show(plot_obj)
